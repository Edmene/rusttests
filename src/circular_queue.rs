use std::io::BufReader;
use std::io::BufRead;
use std::io::Write;
use std::io::stdin;

fn main() {
    let mut row:Vec<i32> = Vec::new();
    for i in 0..5{
        row.push(0);
    }

    operation(&mut row);

}

fn operation(row:&mut Vec<i32>){
    let mut limiters:Vec<i32> = Vec::new();
    for i in 0..2{
        limiters.push(-1);
    }
    for i in 0..9 {
        let mut selection = String::new();
        stdin().read_line(&mut selection).expect("failed to read line");
        let mut selection: i32 = selection.trim().parse()
            .expect("Please type a number!");
        match selection{
            1 => {remove(row, &mut limiters)},
            _ =>  {include(row, selection, &mut limiters);},
        }

    }

    for r in row{
        println!("{}", r);
    }
}

fn include(row:&mut Vec<i32>,value:i32,limiters:&mut Vec<i32>){
    if (limiters[0] == 4 && limiters[1] <= 0) ||
        (limiters[0] != 4 && limiters[0]+1==limiters[1]){
        println!("Overflow of queue.");
    }
    else{
        if limiters[0] == 4{
            limiters[0]=0;
        }
        else {
            limiters[0]=limiters[0]+1;
        }
        let mut index = 0;
        for i in 0..limiters[0]{
            index=index+1;
        }
        row[index] = value;
    }

}

fn remove(row:&mut Vec<i32>, limiters:&mut Vec<i32>){
    let mut torf:bool = false;
    if limiters[0] == -1{
        torf=true;
    }
    match torf{
        true => {print!("Underflow of queue.");},
        false => {
            if limiters[0] == limiters[1]{
                limiters[0] = -1;
                limiters[1] = -1;
            }
            else {
                if limiters[1] == 4 {
                    limiters[1] = 0;
                } else {
                    limiters[1] = limiters[1] + 1
                }
            }
            let mut index = 0;
            let mut a:String = String::new();
            for i in 0..limiters[1]{
                index=index+1;
            }
            row[index] = 0;
        }
        _ => {},
    }
    


}