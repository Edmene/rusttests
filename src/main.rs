extern crate rand;
extern crate regex;
extern crate ifaces;

use rand::prelude::*;
use std::io::{stdin};
use std::net::{UdpSocket, Ipv4Addr, SocketAddrV4};
use std::sync::{mpsc, Arc, Mutex};
use std::thread;
use regex::Regex;
use std::str::FromStr;

fn main() {
    let mut port = thread_rng().gen_range(0, 240);
    port = port + 50000;

    let port_send = port + 5;

    let (tx, rx) = mpsc::channel();

    let cli_vec: Arc<Mutex<Vec<String>>> = Arc::new(Mutex::new(vec!()));

    thread::spawn(move || {
        receive_message(port, tx);
    });

    {
        let cli_vec = cli_vec.clone();

        thread::spawn(move || {
            loop {
                match rx.recv() {
                    Ok(address) => {
                        let mut vec = cli_vec.lock().unwrap();
                        if !vec.contains(&address) {
                            vec.push(address);
                        }
                    }
                    Err(_e) => (),
                }
            }
        });
    }
    let cli_vec = cli_vec.clone();
    send_message(port_send, cli_vec);


}


fn receive_message(port: i32, sender: mpsc::Sender<String>) {
    let mut buf = [0 as u8; 516];

    let mut address = "0.0.0.0:".to_owned();
    address.push_str(port.to_string().as_str());

    let socket = UdpSocket::bind(address).unwrap();

    loop {
        match socket.recv(&mut buf) {
            Ok(_received) => {
                let mut sender_address = String::new();

                for i in 510..514 {
                    sender_address.push_str(buf[i].to_string().as_str());
                    if i != 513 {
                        sender_address.push_str(".");
                    }
                    else {
                        sender_address.push_str(":")
                    }
                }

                let port = buf[515] as i32 + 50000;

                sender_address.push_str(port.to_string().as_str());

                match sender.send(sender_address) {
                    Ok(_sender) => {
                    }
                    Err(_e) => println!("A error has occur {}", _e),
                }
                if buf[0] == 0 {
                    let mut texto = Vec::new();
                    for i in 1..510 {
                        if buf[i] == 0 {
                            break;
                        }
                        texto.push(buf[i]);
                    }

                    println!("Mensagem: {}", String::from_utf8(texto).unwrap());
                }
            }
            Err(e) => println!("recv failed: {:?}", e)
        }
    }
}

fn send_message(port: i32, cli_vec: Arc<Mutex<Vec<String>>>) {

    let re = Regex::new(r"(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}):(\d{4,5})").unwrap();

    let mut eth_address = Option::None;
    for iface in
        ifaces::Interface::get_all().unwrap()
            .into_iter() {
        if iface.name.contains("en") && iface.kind == ifaces::Kind::Ipv4 {
            eth_address = iface.addr;
        }
        if iface.name.contains("wl") && iface.kind == ifaces::Kind::Ipv4
            && eth_address.is_none(){
            eth_address = iface.addr;
        }
    }

    let mut address = "0.0.0.0:".to_owned();
    address.push_str((port).to_string().as_str());

    let mut address_recv = "0.0.0.0:".to_owned();
    address_recv.push_str((port-5).to_string().as_str());

    let socket = UdpSocket::bind(address.to_owned()).unwrap();

    println!("Escutando no endereco {}", address_recv);

    loop {
        let mut menu = String::new();
        menu.push_str("-----------------------");
        menu.push_str("\nEscolha uma das funcoes");
        menu.push_str("\n1) Informar endereco");
        menu.push_str("\n2) Enviar mensagem");
        menu.push_str("\n3) Sair");
        menu.push_str("\n-----------------------\n");
        println!("{}", menu);
        let mut entrada = String::new();
        stdin().read_line(&mut entrada).expect("Erro ao ler linha");
        entrada.pop().unwrap();


        match entrada.as_str().parse::<u8>() {
            Ok(op) => {
                match op {
                    1 => {
                        println!("Lendo endereco");
                        let mut valid = false;
                        while !valid {
                            let mut address = String::new();
                            stdin().read_line(&mut address).expect("Erro ao ler linha");
                            address.pop().unwrap();

                            valid = re.is_match(address.as_str());

                            if valid {
                                match cli_vec.try_lock() {
                                    Ok(mut vec) => {
                                        if !vec.contains(&address) {
                                            vec.push(address);
                                        }
                                    },
                                    Err(_e) => println!("Failed to acquire lock {}", _e)
                                }
                            }
                            else {
                                println!("Endereco invalido");
                            }
                        }

                    },

                    2 => {
                        println!("Lendo mensagem");
                        let mut entrada = String::new();
                        stdin().read_line(&mut entrada).expect("Erro ao ler linha");
                        entrada.pop().unwrap();

                        let mut buf = [0 as u8; 516];

                        for i in 1..510 {
                            if i <= entrada.len() {
                                buf[i] = entrada.as_bytes()[i-1];
                            }
                        }
                        let mut address_ck = vec!();

                        match cli_vec.try_lock() {
                            Ok(vec) => {

                                for address in vec.to_vec() {
                                    address_ck.push(address.to_owned());

                                    socket.connect(address).expect("connect function failed");
                                    let local_address = socket.local_addr().unwrap();
                                    buf[0] = 0;
                                    buf[515] = (local_address.port()-50005) as u8;
                                    let ip = Ipv4Addr::from_str(
                                        eth_address.unwrap().ip().to_string().as_str())
                                        .unwrap().octets();
                                    for i in 510..514 {
                                        buf[i] = ip[i - 510];
                                    }

                                    match socket.send(&buf) {
                                        Ok(_result) => {}
                                        Err(e) => println!("send function failed: {:?}", e),
                                    }

                                }
                            },
                            Err(_e) => println!("Failed to acquire lock {}", _e)
                        }

                        match cli_vec.try_lock() {
                            Ok(vec) => {
                                for i in 0..address_ck.len() {

                                    for host in vec.to_vec() {
                                        if host != address_ck[i] {
                                            let host = host.as_str().parse::<SocketAddrV4>().unwrap();
                                            let ip = host.ip().octets();
                                            let port = host.port() - 50000;

                                            let mut buf_con = [0 as u8; 516];
                                            buf_con[0] = 1;
                                            buf_con[515] = port as u8;
                                            for i in 510..514 {
                                                buf_con[i] = ip[i - 510];
                                            }

                                            socket.connect(address_ck[i].to_owned()).expect("connect function failed");

                                            match socket.send(&buf_con) {
                                                Ok(_result) => {}
                                                Err(e) => println!("send function failed: {:?}", e),
                                            }
                                        }
                                    }
                                }
                            },
                            Err(_e) => println!("Failed to acquire lock {}", _e)

                        }
                    },

                    3 => {
                        break;
                    },

                    _ => {
                        println!("Opcao invalida");
                        continue
                    }
                }
            },
            _ => {
                continue
            }
        }
    }
}
