use std::io::stdin;

#[derive(Clone)]
struct Node {
    info : String,
    link : i8,
}

struct List {
    nodes : Vec<Node>,
    available : i8,
    head_node : i8,
}

impl Node{
    pub fn new() -> Node {
        Node {
            info: "".to_string(),
            link: 0 as i8,
        }
    }
}

impl List{

    pub fn new(size:usize) -> List{
        let mut node_list:Vec<Node> = Vec::new();
        for i in 1..size+1 {
            let mut node:Node = Node::new();
            node.link = i as i8;
            node_list.push(node.clone());
        }
        node_list[size-1].link=-1;
        node_list[0].link=-1;
        node_list[0].info=format!("");
        List{
            nodes : node_list,
            available : 1,
            head_node : 0,
        }
    }

    fn acquire_link(&mut self) -> i8{
        if self.available == -1{
            return -1;
        }
        let aux = self.available.clone();
        let link = &self.nodes;
        let index:usize = self.available as usize;
        self.available = link[index].link;
        return aux;
    }

    fn release_link(&mut self,index:i8){
        self.nodes[index as usize].link = self.available.clone();
        self.available = index;
    }

    fn include_information(&mut self, value:String) -> bool{
        let mut next:i8;
        let mut previous:i8;
        let index:i8;
        index = self.acquire_link();
        if index == -1{
            return false;
        }
        else{
            self.nodes[index as usize].info = value.clone();
            previous = self.head_node.clone();
            next = self.nodes[self.head_node as usize].link;
            while next != -1 as i8{
                if value < self.nodes[next as usize].info{
                    break;
                }
                previous = next.clone();
                next = self.nodes[next as usize].link;
            }
            self.nodes[index as usize].link = next;
            self.nodes[previous as usize].link = index;
            if next == -1{
                let a:String = format!("{}", index);
                self.nodes[self.head_node as usize].info = a;
            }

            return true;
        }

    }

    fn remove_information(&mut self, value:String) -> bool{
        let mut previous:i8;
        let mut index:i8;
        if self.nodes[self.head_node as usize].link == -1{
            return false;
        }
        else {
            previous = self.head_node.clone();
            index = self.nodes[self.head_node as usize].link;
            while index != -1 {
                if value == self.nodes[index as usize].info{
                    self.nodes[index as usize].info = "".to_string();
                    break
                }
                previous = index.clone();
                index = self.nodes[index as usize].link;
            }
            if index == -1{
                println!("Value not found");
            }
            else{
                self.nodes[previous as usize].link = self.nodes[index as usize].link;
                if self.nodes[self.head_node as usize].link == -1{
                    self.nodes[self.head_node as usize].info = format!("-1");
                }
                else {
                    if self.nodes[index as usize].link == -1{
                        self.nodes[self.head_node as usize].info = format!("{}",previous);
                    }
                    self.release_link(index)
                }
            }
            return true;
        }


    }

    fn logical_remove(&mut self, op:u8) -> bool{
        let mut previous:i8;
        let mut index:i8;
        if self.nodes[self.head_node as usize].link == -1{
            return false;
        }
        else {
            previous = self.head_node.clone();
            index = self.nodes[self.head_node as usize].link;
            match op {
                0 => {
                    self.nodes[index as usize].info = format!("");
                }

                1 => {
                    while index != -1 {
                        if self.nodes[index as usize].link == -1{
                            self.nodes[index as usize].info = format!("");
                            break;
                        }
                        previous = index.clone();
                        index = self.nodes[index as usize].link;
                    }
                }

                _ => {}
            }
            self.nodes[previous as usize].link = self.nodes[index as usize].link;
            if self.nodes[self.head_node as usize].link == -1{
                self.nodes[self.head_node as usize].info = format!("-1");
            }
            else{
                if self.nodes[index as usize].link == -1{
                    self.nodes[self.head_node as usize].info = format!("{}", previous);
                }
                self.release_link(index);
            }

            return true;
        }

    }

}

fn main(){
    let mut list:List = List::new(5);
    let mut exit = false;
    while !exit{
        menu();
        let mut selection = String::new();
        stdin().read_line(&mut selection).expect("failed to read line");
        if selection.clone().contains("-1")  {
            exit=true;
        }
        else {
            operation_handling(&mut list, selection.clone());
        }
    }

}

fn operation_handling(list:&mut List, op:String){
    let mut op_result:bool = true;
    if op.contains("0") || op.contains("1") {
        let mut selection = String::new();
        stdin().read_line(&mut selection).expect("failed to read line");
        if op.contains("0"){
            op_result = list.include_information(selection);
        }
        else{
            op_result = list.remove_information(selection);
        }
    }
    if op.contains("2") {
        op_result = list.logical_remove(0);
    }
    if op.contains("3") {
        op_result = list.logical_remove(1);
    }
    if op.contains("4") {
        print_list(list)
    }

    if op_result == false{
        if op.contains("0"){
            println!("Overflow");
        }
        else{
            println!("Underflow");
        }
    }

}

fn print_list(list:&mut List){
    let control_variables = format!("Disp: {} | NC: {}", list.available, list.head_node);
    println!("{}",control_variables);
    println!("-------------------------");
    for node in list.nodes.clone(){
        let node_information = format!("{} | {}",node.info, node.link);
        println!("{}",node_information);
        println!("-------------------------");
    }
}

fn menu(){
    let menu_strings = ["Selecao", "0) Entrada", "1) Retirada por nome",
        "2) Retirada lógica inicio", "3) Retirada lógica fim",
        "4) Imprimir ordem fisica", "-1) Sair","Op: "];
    for string in menu_strings.iter(){
        println!("{}",string);
    }
}