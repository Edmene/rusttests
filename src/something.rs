use std::io::stdin;

#[derive(Clone)]
struct Node {
    previous_link : i8,
    info : String,
    next_link : i8,
}

struct List {
    nodes : Vec<Node>,
    available : i8,
    head_node : i8,
}

impl Node{
    pub fn new() -> Node {
        Node {
            info: "".to_string(),
            previous_link: 0 as i8,
            next_link: 0 as i8,
        }
    }
}

impl List{

    pub fn new(size:usize) -> List{
        let mut node_list:Vec<Node> = Vec::new();
        for i in 1..size+1 {
            let mut node:Node = Node::new();
            node.next_link = i as i8;
            node_list.push(node.clone());
        }
        node_list[size-1].next_link=-1;
        node_list[0].next_link=0;
        node_list[0].info=format!("");
        List{
            nodes : node_list,
            available : 1,
            head_node : 0,
        }
    }

    fn acquire_link(&mut self) -> i8{
        if self.available == -1{
            return -1;
        }
        let aux = self.available.clone();
        let link = &self.nodes;
        let index:usize = self.available as usize;
        self.available = link[index].next_link;
        return aux;
    }

    fn release_link(&mut self,index:i8){
        self.nodes[index as usize].next_link = self.available.clone();
        self.available = index;
    }

    fn include_information(&mut self, value:String) -> bool{
        let mut next:i8;
        let index:i8;
        index = self.acquire_link();
        if index == -1{
            return false;
        }
        else{
            self.nodes[index as usize].info = value.clone();
            next = self.nodes[self.head_node as usize].next_link;
            while next != self.head_node as i8{
                if value < self.nodes[next as usize].info{
                    break;
                }
                next = self.nodes[next as usize].next_link;
            }
            self.nodes[index as usize].next_link = next;
            self.nodes[index as usize].previous_link = self.nodes[next as usize].previous_link;
            let previous_link = self.nodes[index as usize].previous_link.clone();
            let next_link = self.nodes[index as usize].next_link.clone();
            self.nodes[previous_link as usize].next_link = index;
            self.nodes[next_link as usize].previous_link = index;
            return true;
        }

    }

    fn remove_information(&mut self, value:String) -> bool{
        let mut index:i8;
        if self.nodes[self.head_node as usize].next_link == self.head_node{
            return false;
        }
        else {
            index = self.nodes[self.head_node as usize].next_link;
            while index != self.head_node {
                if value == self.nodes[index as usize].info{
                    self.nodes[index as usize].info = "".to_string();
                    break
                }
                index = self.nodes[index as usize].next_link;
            }
            if index == self.head_node{
                println!("Value not found");
            }
            else{
                let previous_link = self.nodes[index as usize].previous_link.clone();
                let next_link = self.nodes[index as usize].next_link.clone();
                self.nodes[previous_link as usize].next_link = self.nodes[index as usize].next_link;
                self.nodes[next_link as usize].previous_link = self.nodes[index as usize].previous_link;
                self.release_link(index);
            }
            return true;
        }


    }

    fn logical_remove(&mut self, op:u8) -> bool{
        let mut index:i8 = 0;
        if self.nodes[self.head_node as usize].next_link == self.head_node{
            return false;
        }
        else {
            match op {
                0 => {
                    index = self.nodes[self.head_node as usize].next_link;
                }
                1 => {
                    index = self.nodes[self.head_node as usize].previous_link;
                }
                _ => {}
            }
            if index == self.head_node{
                println!("Value not found.");
            }
            else{
                self.nodes[index as usize].info = format!("");
                let previous_link = self.nodes[index as usize].previous_link.clone();
                let next_link = self.nodes[index as usize].next_link.clone();
                self.nodes[previous_link as usize].next_link = self.nodes[index as usize].next_link;
                self.nodes[next_link as usize].previous_link = self.nodes[index as usize].previous_link;
                self.release_link(index);
            }

            return true;
        }

    }

}

fn main(){
    let mut list:List = List::new(5);
    let mut exit = false;
    while !exit{
        menu();
        let mut selection = String::new();
        stdin().read_line(&mut selection).expect("failed to read line");
        if selection.clone().contains("-1")  {
            exit=true;
        }
        else {
            operation_handling(&mut list, selection.clone());
        }
    }

}

fn operation_handling(list:&mut List, op:String){
    let mut op_result:bool = true;
    if op.contains("0") || op.contains("1") {
        let mut selection = String::new();
        stdin().read_line(&mut selection).expect("failed to read line");
        if op.contains("0"){
            op_result = list.include_information(selection);
        }
        else{
            op_result = list.remove_information(selection);
        }
    }
    if op.contains("2") {
        op_result = list.logical_remove(0);
    }
    if op.contains("3") {
        op_result = list.logical_remove(1);
    }
    if op.contains("4") {
        print_list(list)
    }

    if op_result == false{
        if op.contains("0"){
            println!("Overflow");
        }
        else{
            println!("Underflow");
        }
    }

}

fn print_list(list:&mut List){
    let control_variables = format!("Disp: {} | NC: {}", list.available, list.head_node);
    println!("{}",control_variables);
    println!("-------------------------");
    for node in list.nodes.clone(){
        let node_information = format!("{} | {} | {}",node.previous_link ,node.info, node.next_link);
        println!("{}",node_information);
        println!("-------------------------");
    }
}

fn menu(){
    let menu_strings = ["Selecao", "0) Entrada", "1) Retirada por nome",
        "2) Retirada lógica inicio", "3) Retirada lógica fim",
        "4) Imprimir ordem fisica", "-1) Sair","Op: "];
    for string in menu_strings.iter(){
        println!("{}",string);
    }
}